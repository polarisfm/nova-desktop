const path = require("path");
const os = require("os");
const findNpmPrefix = require("find-npm-prefix");
const { copy, remove } = require("fs-extra");
const isCI = require("is-ci");

async function main() {
  const prefix = await findNpmPrefix(process.cwd());
  let packageJson;
  try {
    packageJson = require(path.join(prefix, "package.json"));
  } catch (e) {
    console.error("Can't find a package.json!");
    process.exit(1);
  }
  const hakDepsCfg = packageJson.hakDependencies || {};
  const hakModulesDir = path.join(".hak", "hakModules");

  for (const dep of Object.keys(hakDepsCfg)) {
    const modulePlatformDir = path.join(
      ".hak",
      "hakModules",
      os.platform(),
      dep
    );
    const moduleDir = path.join(hakModulesDir, dep);
    console.log(`copying ${modulePlatformDir} to ${moduleDir}`);
    await copy(modulePlatformDir, moduleDir);
  }

  /**
   * isCI is used to recognise when we are building on ToDesktop.
   *
   * If we're building on ToDesktop then we clean up the unused native modules
   * so that they are not bundled with our app.
   */
  if (!isCI) {
    console.log("We are running in CI (e.g ToDesktop)");
    for (const platform of ["darwin", "win32", "linux"]) {
      console.log(`Deleting unused native modules for ${platform}`);
      await remove(path.join(hakModulesDir, platform));
    }
  }
}

main();
